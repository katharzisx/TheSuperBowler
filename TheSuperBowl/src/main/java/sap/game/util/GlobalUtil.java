package sap.game.util;

//import java.io.File;
import java.util.Scanner;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
//import org.apache.log4j.PropertyConfigurator;

//Single shared utilities handler instance 
public class GlobalUtil {
	private static GlobalUtil utilHandler = null;
	Logger logger = Logger.getLogger(GlobalUtil.class);
	private GlobalUtil() {
        BasicConfigurator.configure();
		//Set the location of log4j properties file
//		String log4jConfigFile = System.getProperty("user.dir")
//                + File.separator + "src/main/resources/log4j.properties";
//        PropertyConfigurator.configure(log4jConfigFile);
        //logSystemConsoleMessage("System test!");
	};
	public static GlobalUtil getInstance() {
		if(utilHandler==null) {
			utilHandler = new GlobalUtil();
		}
		return utilHandler;
	}
	
	/**
	 * Method to show message to player
	 * @param consoleMessage
	 */
	public void logPlayerOutputConsoleMessage(String consoleMessage) {
		System.out.println(consoleMessage);
	}
	
	/**
	 * Method for system log file
	 * @return
	 */
	public String logPlayerInputConsoleMessage() {
		Scanner scanner = new Scanner(System.in);
		String userInputData = scanner.nextLine();
		return userInputData;
		
	}
	
	/**
	 * Method for system log file
	 * @return
	 */
	public void logSystemConsoleMessage(String msgtitle, Object data) {
		String logMsg = "{ "+msgtitle+" } : { "+String.valueOf(data)+" }";
		//System.out.println(logMsg);
		logger.debug(logMsg);
	}
	
	public void logSystemConsoleMessage(String sMessage) {
		String logMsg = "{ "+sMessage+" } ";
		//System.out.println(logMsg);
		logger.debug(logMsg);
	}
	
}
