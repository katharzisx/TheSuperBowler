package sap.game;

import java.util.Random;

import org.hamcrest.core.IsInstanceOf;

import sap.game.core.PlayGame;
import sap.game.core.Player;
import sap.game.util.GlobalUtil;

public class TheLonelyBowler {
	PlayGame singlePlayerGame = null;
	GlobalUtil utilHandler = GlobalUtil.getInstance();
	Player playerData = null;
	String userInput = null;
	//AutoSim flag
	Boolean isAutoToss = true;
	
	
	public TheLonelyBowler(Player playerData) {
		this.playerData = playerData;
		//start a new game
		simulateNewBowlingGame();
	}
	
	/**
	 * Method to simulate each toss per round
	 * isAutoToss controls random toss generation
	 * or toss input taken from user
	 */
	public void simulateNewBowlingGame() {
		this.singlePlayerGame = new PlayGame();
		int boundToGenerateToss = 10;
		Integer numberOfPinsDowned = null;
		while(!this.singlePlayerGame.getIsGameOver()) {
			userInput = null;
			numberOfPinsDowned = null;
			if(isAutoToss) {
				utilHandler.logPlayerOutputConsoleMessage("Hit enter to simulate the toss! for "+singlePlayerGame.getCurrentRoundAndToss());
				//read user enter command
				readUserInput();
				//generate number in accordance to the allowed bounds
				numberOfPinsDowned = getNextTossNumber(boundToGenerateToss);
			}else {
				utilHandler.logPlayerOutputConsoleMessage("Please enter a number input from 0 to "+boundToGenerateToss+" for "+singlePlayerGame.getCurrentRoundAndToss());
				//read user enter command
				readUserInput();
				if(userInput!=null) {
					//while(! (numberOfPinsDowned!=null && numberOfPinsDowned>=0 && numberOfPinsDowned <= boundToGenerateToss)) {
						//readUserInput();
						try {
							numberOfPinsDowned = Integer.valueOf(userInput);
							if(numberOfPinsDowned<0 && numberOfPinsDowned>boundToGenerateToss) {
								throw new NumberFormatException();
							}
						} catch (NumberFormatException e) {
							utilHandler.logPlayerOutputConsoleMessage("Please enter a valid number input from 0 to "+boundToGenerateToss);
						}
					//}
				}
			}
			/**
			 * Toss Ball
			 */
			if((numberOfPinsDowned instanceof Integer) && numberOfPinsDowned!=null && (numberOfPinsDowned<=boundToGenerateToss)){
				utilHandler.logPlayerOutputConsoleMessage("Number of pins knocked down:"+numberOfPinsDowned+"\n");
				int[] nextTossData = singlePlayerGame.tosstheBall(numberOfPinsDowned);
				boundToGenerateToss = nextTossData[0];
			}
		}
		
		utilHandler.logPlayerOutputConsoleMessage("\n\t PLAYER "+playerData.getNickName() +"'s FINAL SCORE : "+singlePlayerGame.getFinalGameScore());
		
	}
	
	
	/**
	 * Get next toss number based on max bounds
	 * @param maxBounds
	 * @return
	 */
	public int getNextTossNumber(int maxBounds) {
		++maxBounds;//needs to be inclusive of max bounds
		int tossPins = 0;
		Random randInt = new Random();
		tossPins = randInt.nextInt(maxBounds);
		return tossPins;
	}
	
	/**
	 * Read user console input
	 */
	public void readUserInput() {
		userInput = utilHandler.logPlayerInputConsoleMessage();
	}
}
