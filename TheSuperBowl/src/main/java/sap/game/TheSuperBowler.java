package sap.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import sap.game.core.PlayGame;
import sap.game.core.Player;
import sap.game.util.GlobalUtil;

public class TheSuperBowler {

	GlobalUtil utilHandler = GlobalUtil.getInstance();
	String userInput = null;
	/**
	 * Bootstrap
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		TheSuperBowler theSuperBowler = new TheSuperBowler();
		theSuperBowler.showGameMenu();
		
	}
	
	public void showGameMenu() {

		Player singlePlayer = new Player();
		utilHandler.logPlayerOutputConsoleMessage("\t Welcome to \"The Super Bowler\" bowling Alley!");
		utilHandler.logPlayerOutputConsoleMessage("\n Hit Enter to start a new game");
		utilHandler.logPlayerOutputConsoleMessage("\nA. TheLonelyBowlers(Single Player) Start a new game.");

		readUserInput();

		//utilHandler.logPlayerOutputConsoleMessage("\n Please enter player information.");
		userInput = "";
		utilHandler.logPlayerOutputConsoleMessage("\n Please enter your nickname!");
		readUserInput();
		while(userInput.length()==0) {
			utilHandler.logPlayerOutputConsoleMessage("\n Please enter a valid nickname!");
			userInput = utilHandler.logPlayerInputConsoleMessage();
		}
		singlePlayer.setNickName(userInput);
		//Start New Game
		TheLonelyBowler theLonelyBowler = new TheLonelyBowler(singlePlayer);
		
	}
	
	public void readUserInput() {
		userInput = utilHandler.logPlayerInputConsoleMessage();
	}

}
