package sap.game.core;

public class Round {
	public Round() {};
	//Ten pins per Round
	public static final int TOTAL_PINS = 10;
	//private int total_pins_bowled = 0;
	//private int total_pins_remaining_after_first_toss = 0;
	//Toss keepers
	public static enum TOSS_STATUS{
		FIRST_TOSS,
		SECOND_TOSS,
		THIRD_TOSS
	}

	TOSS_STATUS currentToss = TOSS_STATUS.FIRST_TOSS;
	//pins score per toss
	private Integer pins_bowled_first_toss = null;
	private Integer pins_bowled_second_toss = null;
	private Integer pins_bowled_third_toss = null;
	//Score per round
	private int roundScore = 0;
	//Check if round over
	private Boolean isRoundOver = false;
	//round started(current toss = first toss)
	private Boolean isRoundStarted = false;
	//IN CASE ITS THE LAST ROUND SET TO TRUE
	private Boolean isLastRound = false;

	//Bonus Point Constant
	public static int BONUS_POINTS = 10;
	//Bonus score checks
	private Boolean isStrike = false;
	//only used for the last round
	private Boolean isSecondStrike = false;
	private Boolean isSpare = false;
	//private Boolean isGutter = false;
	//Score from future round
	private int roundFinalScore = 0;

	//flag to check if there are rounds with spare/strike for which score needs to be computed
	private Boolean isScoreNeedsToBeComputed = false;
	
	/**
	 * Computes round score if not strike or spare
	 * @return
	 */
	public int computeRoundScore() {

		//In case all bonus parameters are false
		if(currentToss==TOSS_STATUS.FIRST_TOSS) {
			this.roundScore = pins_bowled_first_toss.intValue();
		}else if(currentToss==TOSS_STATUS.SECOND_TOSS) {
			this.roundScore = pins_bowled_first_toss.intValue()+pins_bowled_second_toss.intValue();
		}
		
		return this.roundScore;
	}
	
	
	//Setters and getters
	public int getRoundFinalScore() {
		return roundFinalScore;
	}
	public void setRoundFinalScore(int roundFinalScore) {
		this.roundFinalScore = roundFinalScore;
	}
	public Boolean getIsSecondStrike() {
		return isSecondStrike;
	}
	public void setIsSecondStrike(Boolean isSecondStrike) {
		this.isSecondStrike = isSecondStrike;
	}
	public Integer getPins_bowled_first_toss() {
		return pins_bowled_first_toss;
	}
	public void setPins_bowled_first_toss(Integer pins_bowled_first_toss) {
		this.pins_bowled_first_toss = pins_bowled_first_toss;
	}
	public Integer getPins_bowled_second_toss() {
		return pins_bowled_second_toss;
	}


	public void setPins_bowled_second_toss(Integer pins_bowled_second_toss) {
		this.pins_bowled_second_toss = pins_bowled_second_toss;
	}


	public Integer getPins_bowled_third_toss() {
		return pins_bowled_third_toss;
	}


	public void setPins_bowled_third_toss(Integer pins_bowled_third_toss) {
		this.pins_bowled_third_toss = pins_bowled_third_toss;
	}
	public Boolean getIsLastRound() {
		return isLastRound;
	}
	public void setIsLastRound(Boolean isLastRound) {
		this.isLastRound = isLastRound;
	}
	public Boolean getIsRoundStarted() {
		return isRoundStarted;
	}
	public void setIsRoundStarted(Boolean isRoundStarted) {
		this.isRoundStarted = isRoundStarted;
	}
	public Boolean getIsScoreNeedsToBeComputed() {
		return isScoreNeedsToBeComputed;
	}
	public void setIsScoreNeedsToBeComputed(Boolean isScoreNeedsToBeComputed) {
		this.isScoreNeedsToBeComputed = isScoreNeedsToBeComputed;
	}
	public Boolean getIsStrike() {
		return isStrike;
	}
	public void setIsStrike(Boolean isStrike) {
		this.isStrike = isStrike;
	}
	public Boolean getIsSpare() {
		return isSpare;
	}
	public void setIsSpare(Boolean isSpare) {
		this.isSpare = isSpare;
	}
	public TOSS_STATUS getCurrentToss() {
		return currentToss;
	}
	public void setCurrentToss(TOSS_STATUS currentToss) {
		this.currentToss = currentToss;
	}

	public int getRoundScore() {
		return roundScore;
	}
	public void setRoundScore(int roundScore) {
		this.roundScore = roundScore;
	}
	public Boolean getIsRoundOver() {
		return isRoundOver;
	}
	public void setIsRoundOver(Boolean isRoundOver) {
		this.isRoundOver = isRoundOver;
	}
//	public void setIsRoundOver(Boolean isRoundOver) {
//		this.isRoundOver = isRoundOver;
//	}
}
