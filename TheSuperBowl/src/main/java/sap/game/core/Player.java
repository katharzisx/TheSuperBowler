package sap.game.core;

/**
 * Player information
 * @author jeff
 *
 */
public class Player {
	private String nickName;
	private int shoeSize;
	
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public int getShoeSize() {
		return shoeSize;
	}
	public void setShoeSize(int shoeSize) {
		this.shoeSize = shoeSize;
	}
}
