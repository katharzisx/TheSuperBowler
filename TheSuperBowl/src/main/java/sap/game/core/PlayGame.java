package sap.game.core;

import java.util.ArrayList;

import sap.game.util.GlobalUtil;

public class PlayGame {

	//Utilities handler
	GlobalUtil utilHandler = GlobalUtil.getInstance();
	//GAME data
	int MAX_ROUNDS = 10;
	//Game Over status
	public Boolean isGameOver = false;

	//will store the index of the old round for which score has been computed
	int indexOfRoundLastComputed = 0;
	//Current round holders
	int currentRoundIndex = 0;
	Round currentRound = null;

	//Initialized SCORE to 0 at game start
	int theSuperBowlerGameScore = 0;

	//GAME ROUNDS
	ArrayList<Round> listGameRounds = new ArrayList<Round>();
	//Main Score board
	int[] scoreBoard = new int[10];
	//0th index will contain next toss data
	int[] dataForNextToss = new int[2];
	public PlayGame() {
		//Initialize Rounds for a new game based on MAX_ROUNDS
		for(int i=1;i<=MAX_ROUNDS;++i) {
			listGameRounds.add(new Round());
		}
		//utilHandler.logSystemConsoleMessage("Number of Game Rounds",listGameRounds.size());

		//Initialize current round with first round
		currentRound = listGameRounds.get(0);
	}

	/**
	 * Invoked for every toss of the bowling ball
	 * @param tossPinsFallen : number of pins dropped for the toss
	 */
	public int[] tosstheBall(int tossPinsFallen) {
		//utilHandler.logSystemConsoleMessage("Toss data:"+tossPinsFallen);
		if(!currentRound.getIsRoundOver()) {
			//for all rounds except the last round
			if(!currentRound.getIsLastRound()) {
				//FIRST TOSS
				if(currentRound.getCurrentToss()==Round.TOSS_STATUS.FIRST_TOSS) {
					//set to true for all rounds that have been started
					currentRound.setIsRoundStarted(true);
					if(tossPinsFallen<10) {
						//Set the pins for first toss
						currentRound.setPins_bowled_first_toss(tossPinsFallen);
						//change current toss for round
						currentRound.setCurrentToss(Round.TOSS_STATUS.SECOND_TOSS);
					}else if(tossPinsFallen==10) {//STRIKE

						currentRound.setIsStrike(true);
						//Set current round status to OVER, for change to next round
						currentRound.setIsRoundOver(true);
						//set score to be computed in future for this round to true
						currentRound.setIsScoreNeedsToBeComputed(true);
						//utilHandler.logSystemConsoleMessage("STRIKE IN CURRENT ROUND "+currentRound.getIsScoreNeedsToBeComputed());
					}else {
						utilHandler.logSystemConsoleMessage("Incorrect Toss Data:"+tossPinsFallen);
					}
				}
				//SECOND TOSS
				else if(currentRound.getCurrentToss()==Round.TOSS_STATUS.SECOND_TOSS) {
					int pinsFirstRound = currentRound.getPins_bowled_first_toss();
					if(tossPinsFallen<(Round.TOTAL_PINS - pinsFirstRound)) {
						//set the number of pins fallen if not spare(not all pins fallen)
						currentRound.setPins_bowled_second_toss(tossPinsFallen);	
						//compute current round score
						currentRound.computeRoundScore();
						//keep index of the round for which score has been already computed
						indexOfRoundLastComputed= currentRoundIndex;
					}else if(tossPinsFallen==(Round.TOTAL_PINS - pinsFirstRound)) {
						//set spare flag to true
						currentRound.setIsSpare(true);
						//set score to be computed in future for this round to true
						currentRound.setIsScoreNeedsToBeComputed(true);
					}else {
						utilHandler.logSystemConsoleMessage("Incorrect Toss Data:"+tossPinsFallen);
					}

					//Set current round status to OVER, for change to next round
					currentRound.setIsRoundOver(true);
				}
			}else {
				/**
				 * FINAL ROUND Logic
				 */
				//FIRST TOSS
				if(currentRound.getCurrentToss()==Round.TOSS_STATUS.FIRST_TOSS) {
					//set to true even if first toss is done
					currentRound.setIsRoundStarted(true);
					if(tossPinsFallen<10) {
						//Set the pins for first toss
						currentRound.setPins_bowled_first_toss(tossPinsFallen);
					}else if(tossPinsFallen==10) {//STRIKE
						//ONLY for last round we are entering value as 10 for the first toss
						//on occurrence of strike
						//this is beneficial for the 9th round's strike scenario
						currentRound.setPins_bowled_first_toss(tossPinsFallen);
						//Special case, last round, will have two more toss attempts in case of strike
						currentRound.setIsStrike(true);
						//set score to be computed in future for this round to true
						currentRound.setIsScoreNeedsToBeComputed(true);
					}
					//change current toss for round (EVEN IF STRIKE OCCURRED)
					currentRound.setCurrentToss(Round.TOSS_STATUS.SECOND_TOSS);
				}
				//SECOND TOSS
				else if(currentRound.getCurrentToss()==Round.TOSS_STATUS.SECOND_TOSS) {
					//strike in first toss
					if(currentRound.getIsStrike()) {
						if(tossPinsFallen<10) {
							//Set the pins for second toss
							currentRound.setPins_bowled_second_toss(tossPinsFallen);
							//change current toss for round
							//allowed since first toss attempt was a strike
							currentRound.setCurrentToss(Round.TOSS_STATUS.THIRD_TOSS);

						}else if(tossPinsFallen==10) { //ANOTHER STRIKE
							//on occurrence of strike
							//this is beneficial for the 9th round's strike scenario
							//Set the pins for second toss
							currentRound.setPins_bowled_second_toss(tossPinsFallen);
							//set second strike to true(only for last round)
							currentRound.setIsSecondStrike(true);
							//allowed since the second toss attempt resulted in ANOTHER STRIKE
							currentRound.setCurrentToss(Round.TOSS_STATUS.THIRD_TOSS);
							//set score to be computed in future for this round to true
							currentRound.setIsScoreNeedsToBeComputed(true);
						}
					}else {
						//calculate remaining pins if first toss was not a strike
						int pinsFirstRound = currentRound.getPins_bowled_first_toss();
						if(tossPinsFallen<(Round.TOTAL_PINS - pinsFirstRound)) {
							//not strike and not spare
							//no need for third toss attempt
							//Set the pins for second toss
							currentRound.setPins_bowled_second_toss(tossPinsFallen);
							//compute current round score(last round)
							currentRound.computeRoundScore();//only considers 1st and 2nd toss attempts
							//Set current round status to OVER
							currentRound.setIsRoundOver(true);
							//set is GAME OVER to true to avoid further toss attempts
							isGameOver = true;
							//game over
						}else if(tossPinsFallen==(Round.TOTAL_PINS - pinsFirstRound)) {
							//set spare flag to true
							currentRound.setIsSpare(true);
							//Set the pins for second toss
							currentRound.setPins_bowled_second_toss(tossPinsFallen);
							//set score to be computed in future for this round to true
							currentRound.setIsScoreNeedsToBeComputed(true);
							//allowed since the second toss attempt resulted in a spare
							currentRound.setCurrentToss(Round.TOSS_STATUS.THIRD_TOSS);
						}else {
							utilHandler.logSystemConsoleMessage("(last round second toss) Incorrect Toss Data:"+tossPinsFallen);
						}
					}
				}//THIRD TOSS
				else if(currentRound.getCurrentToss()==Round.TOSS_STATUS.THIRD_TOSS) {
					//third toss allowed/disallowed controlled by isGameOver
					//third toss allowed, additional check
					if(currentRound.getIsStrike() || currentRound.getIsSpare()) {

						if(tossPinsFallen==10) {
							//STRIKE
							//Set the pins for THIRD toss WILL SET To 10 since we only take its value
							//additional logic if needed
							currentRound.setPins_bowled_third_toss(tossPinsFallen);
						}else {
							//same as above
							currentRound.setPins_bowled_third_toss(tossPinsFallen);
						}
						//The End
						currentRound.setIsRoundOver(true);
						isGameOver = true;
					}else {
						//will not attempt third toss
					}
				}
			}
		}

		//generate score board based on available information
		generateScoreBoard();
		//check and set current round status
		checkAndSetCurrentRoundAndSetNextTossParams();
		//Return data required to select next toss
		return dataForNextToss;
	}

	/**
	 * iterate thru the rounds and compute scores
	 * for the rounds which had a spare/strike and have score dependency on future rounds
	 * If score has already been computed for each round
	 * then update the Score Board
	 * 
	 */
	public void generateScoreBoard() {
		//update startIndex so as to avoid unnecessary iterations
		int startIndex = 0;
		//iterate from start index to current round index
		for(int i = startIndex;i<=currentRoundIndex;++i) {
			Round roundData = listGameRounds.get(i);

			//Score computation ONLY for the last round i = 9
			//for strike and spare cases
			scoreComputationsForFinalRoundWithStrikeSpare(roundData);

			//For all rounds for which score needs to be computed: rounds with strike and spare
			scoreComputationForAllRoundsWithStrikeSpare(roundData, i);

			/**
			 * GENERATE SCORE BOARD
			 */
			//if score has already been computed
			//generate score board
			if(!roundData.getIsScoreNeedsToBeComputed()) {
				if(i==0) {
					scoreBoard[i] = roundData.getRoundScore();
				}else {
					scoreBoard[i] = scoreBoard[i-1] + roundData.getRoundScore();
				}				
				listGameRounds.get(i).setRoundFinalScore(scoreBoard[i]);
			}
		}
	}

	/**
	 * Computes Round score for the last round
	 * in case of strike or spare occurrences
	 * @param roundData
	 */
	public void scoreComputationsForFinalRoundWithStrikeSpare(Round roundData) {
		//Score computation ONLY for the last round i = 9
		//for strike and spare cases
		if(roundData.getIsLastRound() && roundData.getIsScoreNeedsToBeComputed()) {
			int bonusToBeAdded = 0;
			//if first toss attempt was a strike then take the value of the next two toss attempts
			//they will be set to 10 in case they both are strikes as well
			if(roundData.getIsStrike() && roundData.getPins_bowled_second_toss()!=null && roundData.getPins_bowled_third_toss()!=null) {
				bonusToBeAdded = roundData.getPins_bowled_second_toss() + roundData.getPins_bowled_third_toss();
				//set round score for current round (10 + bonus to be added)
				int roundScore = Round.TOTAL_PINS + bonusToBeAdded;
				roundData.setRoundScore(roundScore);
				roundData.setIsScoreNeedsToBeComputed(false);
			}else if(roundData.getIsSpare() && roundData.getPins_bowled_third_toss()!=null) {
				//if spare then bonus will take value of the third toss
				bonusToBeAdded = roundData.getPins_bowled_third_toss();
				//set round score for current round (10 + bonus to be added)
				int roundScore = Round.TOTAL_PINS + bonusToBeAdded;
				roundData.setRoundScore(roundScore);
				roundData.setIsScoreNeedsToBeComputed(false);
			}
		}
	}

	/**
	 * Computes Round score for all rounds
	 * with strike/spare
	 * @param roundData
	 * @param i : index
	 */
	public void scoreComputationForAllRoundsWithStrikeSpare(Round roundData, int i) {
		if(roundData.getIsScoreNeedsToBeComputed()) {
			//SPARE
			//then score = 10 + next round first toss points
			//will execute only till/including the 9th round;i< (max_rounds - 1) = 9, i = 0:8 {8th index is the 9th Round}
			if(roundData.getIsSpare() && i<(MAX_ROUNDS - 1)) {
				int bonusToBeAdded = 0;
				Round nextRound = listGameRounds.get(i+1);
				//round is started if FirstToss has been made
				if(nextRound.getIsRoundStarted()) {
					if(nextRound.getIsStrike()) {
						bonusToBeAdded = Round.BONUS_POINTS;//set to 10
					}
					//if spare or any number of pins take the first toss pin number
					else{
						bonusToBeAdded = nextRound.getPins_bowled_first_toss();
					}
					//set round score for current round (10 + bonus to be added)
					int roundScore = Round.TOTAL_PINS + bonusToBeAdded;
					roundData.setRoundScore(roundScore);
					//flag score to be computed to FALSE after score computation is over 
					//for this round
					roundData.setIsScoreNeedsToBeComputed(false);
				}
			}
			//STRIKE
			//score = 10 + points from next two tosses
			//will execute only till/including the 9th round;i< (max_rounds - 1) = 9, i = 0:8 {8th index is the 9th Round}
			else if(roundData.getIsStrike() && i<(MAX_ROUNDS - 1)) {
				int bonusToBeAdded = 0;
				Round nextRound = listGameRounds.get(i+1);
				//only if next round is started
				if(nextRound.getIsRoundStarted()) {
					//case where the next two tosses DO NOT result in Strike/Spare
					if(!nextRound.getIsStrike() && !nextRound.getIsSpare() && nextRound.getPins_bowled_first_toss()!=null && nextRound.getPins_bowled_second_toss()!=null) {
						bonusToBeAdded = nextRound.getPins_bowled_first_toss() + nextRound.getPins_bowled_second_toss();
						//set round score for current round (10 + bonus to be added)
						int roundScore = Round.TOTAL_PINS + bonusToBeAdded;
						roundData.setRoundScore(roundScore);

						roundData.setIsScoreNeedsToBeComputed(false);
						//utilHandler.logSystemConsoleMessage("CHECKING STRIKE CASE:"+roundData.getIsScoreNeedsToBeComputed());
					}else if(nextRound.getIsSpare()) {//also if current round is 9 and the 10th round is a spare
						bonusToBeAdded = Round.TOTAL_PINS;
						//set round score for current round (10 + bonus to be added)
						int roundScore = Round.TOTAL_PINS + bonusToBeAdded;
						roundData.setRoundScore(roundScore);
						roundData.setIsScoreNeedsToBeComputed(false);
					}
					//Multiple cases if next round is strike and
					//1 : The next2next(i+2)th round is between Round 3->9; i.e. i+2 = 1:8
					//2 : nextRound(i+1=8) is 9th and the next2next round is the 10th round(i=9) but it is not a strike
					//3 : nextRound(i+1=8) is 9th and the next2next round is the 10th round(i=9) with the first toss a spare
					//4 : currentRound(i=8) is 9th and the next round is the 10th round, so we consider both the tosses
					//	  i.e first two tosses can be strike or first strike and next spare or both numbers adding to less than 10

					else if(nextRound.getIsStrike()) {
						//10th round also will be striked even if first toss is strike

						//If current round is the 9th round and next round is the 10th round
						//4 : currentRound(i=8) is 9th and the next round is the 10th round, so we consider both the tosses for the 10th round
						//ONLY IF THE 10th rounds first toss and second toss has been assigned
						//just for the 10th round if the first toss is a strike and the second toss is also a strike
						//we are setting their values to 10
						if(i==8 && nextRound.getIsLastRound() && nextRound.getPins_bowled_first_toss()!=null && nextRound.getPins_bowled_second_toss()!=null) {
							bonusToBeAdded = bonusToBeAdded + nextRound.getPins_bowled_first_toss() + nextRound.getPins_bowled_second_toss();
							//set round score for current round (10 + bonus to be added)
							int roundScore = Round.TOTAL_PINS + bonusToBeAdded;
							roundData.setRoundScore(roundScore);
							roundData.setIsScoreNeedsToBeComputed(false);
						}else {
							//10th rounds second toss value is pending
						}

						//if current round is between 1 -> 8th (i = 0:7)
						//then we consider the next2nextround (which may or may not be the 10th round)
						//1 : The next2next(i+2)th round is between Round 3->10; i.e. i+2 = 2(3rd):9(10th) 
						if(i<8) {
							bonusToBeAdded = Round.TOTAL_PINS;//since nextRound is a strike we add 10 
							Round nextToNextRound = listGameRounds.get(i+2);
							//even if the next2next is the 10th we only care about its first toss value
							//which will be set if the round has started
							if(nextToNextRound.getIsRoundStarted()) {
								if(nextToNextRound.getIsStrike()) {
									//since the next2next round is also a strike we again add 10
									bonusToBeAdded = bonusToBeAdded + Round.TOTAL_PINS;
								}else{
									//even if the next2Next round 
									//is spare or non-strike value
									//we only consider the first toss of the next2Next Round
									bonusToBeAdded = bonusToBeAdded + nextToNextRound.getPins_bowled_first_toss();
								}
								//set round score for current round (10 + bonus to be added)
								int roundScore = Round.TOTAL_PINS + bonusToBeAdded;
								roundData.setRoundScore(roundScore);
								roundData.setIsScoreNeedsToBeComputed(false);
							}
						}
					}else {
						//utilHandler.logSystemConsoleMessage("[generateScoreBoard] NextRound!");
					}
				}
			}
		}
	}



	/**
	 * Check/Assign status of currentRound
	 */
	public void checkAndSetCurrentRoundAndSetNextTossParams() {
		//Display updated score board
		displayScoreBoard();
		//Check if current round is over
		//set next round to current round
		if(currentRound.getIsRoundOver() && currentRoundIndex < MAX_ROUNDS-1) {
			++currentRoundIndex;
			currentRound = listGameRounds.get(currentRoundIndex);
			//If it is the last round
			if(currentRoundIndex == (MAX_ROUNDS -1)) {
				currentRound.setIsLastRound(true);
			}
			//Generate next toss data
			dataForNextToss[0] = Round.TOTAL_PINS;
		}else {
			

			if(currentRound.getIsLastRound()) {
				//First is strike
				if(currentRound.getIsStrike()) {
					//second toss attempt is not done
					if(currentRound.getPins_bowled_second_toss()==null) {
						dataForNextToss[0] = Round.TOTAL_PINS;
					}//second done
					else {
						//second is also a strike
						if(currentRound.getIsSecondStrike()) {
							dataForNextToss[0] = Round.TOTAL_PINS;
						}else {
							//only remaining pins for next round
							dataForNextToss[0] = Round.TOTAL_PINS - currentRound.getPins_bowled_second_toss();

						}
					}
				}
				//First is not a strike
				else {
					//second toss attempt is not done
					if(currentRound.getPins_bowled_second_toss()==null && currentRound.getPins_bowled_first_toss()!=null) {
						dataForNextToss[0] = Round.TOTAL_PINS - currentRound.getPins_bowled_first_toss();
					}//second toss done
					else {
						//in second toss we hit all remaining pins
						if(currentRound.getIsSpare()) {
							dataForNextToss[0] = Round.TOTAL_PINS;
						}else {
							//then no third toss
						}
					}
				}
			}
			//IF NOT LAST ROUND
			else {
				//if current round is not over set the toss data parameters for the next toss
				dataForNextToss[0] = Round.TOTAL_PINS - currentRound.getPins_bowled_first_toss();
			}

		
		}
	}

	/**
	 * To get the game score
	 * @return
	 */
	public Integer getFinalGameScore() {
		//		for(int i = 0; i<scoreBoard.length;++i) {
		//			utilHandler.logSystemConsoleMessage(i+ "th round score"+scoreBoard[i]);
		//		}
		theSuperBowlerGameScore += scoreBoard[MAX_ROUNDS-1];
		return theSuperBowlerGameScore;
	}

	/**
	 * Method to check GAME OVER status
	 * to allow/disallow further tosses
	 * @return
	 */
	public Boolean getIsGameOver() {
		return isGameOver;
	}

	public void displayScoreBoard() {
		StringBuilder sbScoreData = new StringBuilder();
		StringBuilder sbRoundTitleData = new StringBuilder();
		StringBuilder sbTossData = new StringBuilder();

		String scoreData = "";
		String throwdata = "";



		for(int i=0;i<=currentRoundIndex;++i) {

			if((currentRound.getIsStrike() || currentRound.getIsSpare()) && scoreBoard[i]==0) {
				scoreData = "TBD";
			}else {
				scoreData = String.valueOf(scoreBoard[i]);
			}
			sbRoundTitleData.append(" || "+(i+1)+"  ");
			sbScoreData.append(" || "+scoreData+" ");
			sbTossData.append(throwdata);


		}
		if(currentRound.getIsRoundOver()) {

			utilHandler.logPlayerOutputConsoleMessage("############################### [ ROUND: "+(currentRoundIndex+1) +"] #####################################");

			//ONLY FOR LAST ROUND SCOREBOARD
			if(currentRound.getIsLastRound()) {
				//1: X 1 3
				//2: X 9 /
				//3: X X X
				//4: X X 9
				//5: 1 / 8
				//6: 1 / X
				//7: 1 7 -
				
				//FIRST STRIKE Cases(will always have third toss data)
				if(currentRound.getIsStrike()) {
					
					utilHandler.logPlayerOutputConsoleMessage("First toss: X");
					if(currentRound.getIsSpare() && currentRound.getPins_bowled_second_toss()!=null && currentRound.getPins_bowled_third_toss()!=null) {
						//2: X 9 /
						utilHandler.logPlayerOutputConsoleMessage("Second toss: "+currentRound.getPins_bowled_second_toss());
						utilHandler.logPlayerOutputConsoleMessage("Third toss: /");
					}
					// X X 
					else if(currentRound.getIsSecondStrike() && currentRound.getPins_bowled_second_toss()==10) {
						utilHandler.logPlayerOutputConsoleMessage("Second toss: X");
						
						//X X X
						if(currentRound.getPins_bowled_third_toss()!=null && currentRound.getPins_bowled_third_toss()==10) {
							utilHandler.logPlayerOutputConsoleMessage("Third toss: X");
						}else if(currentRound.getPins_bowled_third_toss()!=null) {
							// X X 9
							utilHandler.logPlayerOutputConsoleMessage("Third toss: "+currentRound.getPins_bowled_third_toss());
						}
					}
					else if(currentRound.getPins_bowled_second_toss()!=null && currentRound.getPins_bowled_third_toss()!=null) {
						//1: X 1 3
						utilHandler.logPlayerOutputConsoleMessage("Second toss: "+currentRound.getPins_bowled_second_toss());
						utilHandler.logPlayerOutputConsoleMessage("Third toss: "+currentRound.getPins_bowled_third_toss());
					}
				}
				//5: 1 / 8
				//6: 1 / X
				else if(currentRound.getIsSpare()) {
					//First is not a strike
					//5: 1 / 8
					
					//there will be a third toss
					if(currentRound.getPins_bowled_second_toss()!=null && currentRound.getPins_bowled_third_toss()!=null) {
						utilHandler.logPlayerOutputConsoleMessage("First toss: "+currentRound.getPins_bowled_first_toss());
						utilHandler.logPlayerOutputConsoleMessage("Second toss: /");
						if(currentRound.getPins_bowled_third_toss()==10) {
							utilHandler.logPlayerOutputConsoleMessage("Third toss: X");
						}else {
							utilHandler.logPlayerOutputConsoleMessage("Third toss: "+currentRound.getPins_bowled_third_toss());
						}
					}
				}
				//7: 1 7 -
				else {
					utilHandler.logPlayerOutputConsoleMessage("First toss: "+currentRound.getPins_bowled_first_toss());
					utilHandler.logPlayerOutputConsoleMessage("Second toss: "+currentRound.getPins_bowled_second_toss());
				}
			}
			//All other rounds
			else {
				if(currentRound.getIsStrike()) {
					utilHandler.logPlayerOutputConsoleMessage("First toss: X");
				}else if(currentRound.getIsSpare()) {
					utilHandler.logPlayerOutputConsoleMessage("First toss: "+currentRound.getPins_bowled_first_toss());
					utilHandler.logPlayerOutputConsoleMessage("Second toss: /");
				}else {
					utilHandler.logPlayerOutputConsoleMessage("First toss: "+currentRound.getPins_bowled_first_toss());
					utilHandler.logPlayerOutputConsoleMessage("Second toss: "+currentRound.getPins_bowled_second_toss());
				}
			}


			utilHandler.logPlayerOutputConsoleMessage("[ROUNDS] "+sbRoundTitleData.toString());
			utilHandler.logPlayerOutputConsoleMessage("[SCORES] "+sbScoreData.toString());
			utilHandler.logPlayerOutputConsoleMessage("#################################################################################");

		}

	}
	
	/**
	 * Get current round status
	 * @return
	 */
	public String getCurrentRoundAndToss() {
		String roundTossStatus = String.valueOf("ROUND: "+(this.currentRoundIndex+1))+" TOSS: "+currentRound.getCurrentToss();
		return roundTossStatus;
	}


}
