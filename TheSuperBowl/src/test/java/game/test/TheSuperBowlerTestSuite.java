package game.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import sap.game.core.PlayGame;

public class TheSuperBowlerTestSuite {
	//Game
	PlayGame playGame = null;
	//Toss Data
	ArrayList<Integer> listOfTosses = null;
	
	
	/**
	 * Game Factory
	 * @return
	 */
	public PlayGame gameFactory() {
		playGame = new PlayGame();
		return playGame;
	}
	
	/**
	 * All zeroes
	 */
	@Test
	public void testAllGutterZeroScenario() {
		
		int maxTosses = 20;
		int tossValue = 0;
		int expectedScore = 0;
		
		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(tossValue);
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	@Before
    public void setUp() {
		playGame = gameFactory();
    }
	/**
	 * No spare, no strike case
	 */
	@Test
	public void testNoSpareStrikeScenario() {
		
		int expectedScore = generateTossWithNoBonus();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * All strikes 
	 */
	@Test
	public void testAllStrikesScenario() {
		
		int expectedScore = generateAllStrikes();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * All Max spares (9,1),last round 3rdtoss: 9
	 */
	@Test
	public void testAllMaxSparesScenario() {

		int expectedScore = generateAllMaxSparesData();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Sample real world case
	 */
	@Test
	public void testSampleScenario() {

		int expectedScore = generateMixedTossData();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Mix max spares and strikes
	 */
	@Test
	public void testMixStrikesMaxSpares() {

		int expectedScore = generateMixStrikesMaxSparesData();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 1
	 */
	@Test
	public void testLastRoundCase_1() {

		int expectedScore = generateLastRound_Case_1();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 2
	 */
	@Test
	public void testLastRoundCase_2() {

		int expectedScore = generateLastRound_Case_2();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 3
	 */
	@Test
	public void testLastRoundCase_3() {

		int expectedScore = generateLastRound_Case_3();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 4
	 */
	@Test
	public void testLastRoundCase_4() {

		int expectedScore = generateLastRound_Case_4();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 5
	 */
	@Test
	public void testLastRoundCase_5() {

		int expectedScore = generateLastRound_Case_5();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 6
	 */
	@Test
	public void testLastRoundCase_6() {

		int expectedScore = generateLastRound_Case_6();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 7
	 */
	@Test
	public void testLastRoundCase_7() {

		int expectedScore = generateLastRound_Case_7();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 8
	 */
	@Test
	public void testLastRoundCase_8() {

		int expectedScore = generateLastRound_Case_8();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 9
	 */
	@Test
	public void testLastRoundCase_9() {

		int expectedScore = generateLastRound_Case_9();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 10
	 */
	@Test
	public void testLastRoundCase_10() {

		int expectedScore = generateLastRound_Case_10();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 11
	 */
	@Test
	public void testLastRoundCase_11() {

		int expectedScore = generateLastRound_Case_11();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 12
	 */
	@Test
	public void testLastRoundCase_12() {

		int expectedScore = generateLastRound_Case_12();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	/**
	 * Last Round Test Case 13
	 */
	@Test
	public void testLastRoundCase_13() {

		int expectedScore = generateLastRound_Case_13();
		int maxTosses = listOfTosses.size();

		for(int i=0;i<maxTosses;++i) {
			playGame.tosstheBall(listOfTosses.get(i));
		}
		int finalScore = playGame.getFinalGameScore().intValue();
		assertEquals(expectedScore, finalScore);
	}
	
	
	/**
	 * TEST DATA GENERATORS
	 */
	
	
	
	/**
	 * All strikes
	 * EXPECTED: 300
	 */
	public int generateAllStrikes() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<12;++i) {
			listOfTosses.add(10);
		}
		return new Integer(300);
	}
	
	/**
	 * 10th ROUND CASE 1
	 * EXPECTED: X X 9 : 299
	 */
	public int generateLastRound_Case_1() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(10);
		listOfTosses.add(10);
		listOfTosses.add(9);
		return new Integer(299);
	}
	
	/**
	 * 10th ROUND CASE 2
	 * EXPECTED: X 9 1 : 289
	 */
	public int generateLastRound_Case_2() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(10);
		listOfTosses.add(9);
		listOfTosses.add(1);
		return new Integer(289);
	}
	
	
	
	/**
	 * 10th ROUND CASE 3
	 * EXPECTED: 1 7 - : 257
	 */
	public int generateLastRound_Case_3() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(1);
		listOfTosses.add(7);
		//listOfTosses.add(1);
		return new Integer(257);
	}
	
	/**
	 * 10th ROUND CASE 4
	 * EXPECTED: 1 9 X : 271
	 */
	public int generateLastRound_Case_4() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(1);
		listOfTosses.add(9);
		listOfTosses.add(10);
		return new Integer(271);
	}
	
	/**
	 * 10th ROUND CASE 5
	 * EXPECTED: 1 9 9 : 270
	 */
	public int generateLastRound_Case_5() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(1);
		listOfTosses.add(9);
		listOfTosses.add(9);
		return new Integer(270);
	}
	
	/**
	 * 10th ROUND CASE 6
	 * EXPECTED: X 1 9 : 281
	 */
	public int generateLastRound_Case_6() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(10);
		listOfTosses.add(1);
		listOfTosses.add(9);
		return new Integer(281);
	}
	
	/**
	 * 10th ROUND CASE 7
	 * EXPECTED: X 1 3 : 275
	 */
	public int generateLastRound_Case_7() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(10);
		listOfTosses.add(1);
		listOfTosses.add(3);
		return new Integer(275);
	}
	
	/**
	 * 10th ROUND CASE 8
	 * EXPECTED: X 0 0 : 270
	 */
	public int generateLastRound_Case_8() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(10);
		listOfTosses.add(0);
		listOfTosses.add(0);
		return new Integer(270);
	}
	
	/**
	 * 10th ROUND CASE 9
	 * EXPECTED: X 0 / : 280
	 */
	public int generateLastRound_Case_9() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(10);
		listOfTosses.add(0);
		listOfTosses.add(10);
		return new Integer(280);
	}
	
	/**
	 * 10th ROUND CASE 10
	 * EXPECTED: 0 / X : 270
	 */
	public int generateLastRound_Case_10() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(0);
		listOfTosses.add(10);
		listOfTosses.add(10);
		return new Integer(270);
	}
	
	/**
	 * 10th ROUND CASE 11
	 * EXPECTED: 0 0 : 240
	 */
	public int generateLastRound_Case_11() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(0);
		listOfTosses.add(0);
		//listOfTosses.add(10);
		return new Integer(240);
	}
	
	/**
	 * 10th ROUND CASE 11
	 * EXPECTED: 0 9 : 258
	 */
	public int generateLastRound_Case_12() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(0);
		listOfTosses.add(9);
		//listOfTosses.add(10);
		return new Integer(258);
	}
	
	/**
	 * 10th ROUND CASE 11
	 * EXPECTED: 0 / 0 : 260
	 */
	public int generateLastRound_Case_13() {
		listOfTosses = new ArrayList<Integer>();
		for(int i=0;i<9;++i) {
			listOfTosses.add(10);
		}
		//Last Round
		listOfTosses.add(0);
		listOfTosses.add(10);
		listOfTosses.add(0);
		return new Integer(260);
	}
	
	/**
	 * Sample Data Mix
	 * EXPECTED: 187
	 */
	public int  generateMixedTossData() {
		listOfTosses = new ArrayList<Integer>();
		listOfTosses.add(10);//strike
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(5);
		listOfTosses.add(5);//spare
		listOfTosses.add(7);
		listOfTosses.add(2);//
		listOfTosses.add(10);//strike
		listOfTosses.add(10);//strike
		listOfTosses.add(10);//strike
		listOfTosses.add(9);
		listOfTosses.add(0);//
		listOfTosses.add(8);
		listOfTosses.add(2);//spare
		//last round
		listOfTosses.add(9);
		listOfTosses.add(1);
		listOfTosses.add(10);
		
		return new Integer(187);
	}
	
	/**
	 * Sample Data Mix
	 * EXPECTED: 190
	 */
	public int generateAllMaxSparesData() {
		listOfTosses = new ArrayList<Integer>();
		
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		//last round
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(9);
		
		return new Integer(190);
	}
	
	/**
	 * Mix strike spares
	 * expected 200
	 * @return
	 */
	public int generateMixStrikesMaxSparesData() {
		listOfTosses = new ArrayList<Integer>();
		
		
		listOfTosses.add(10);//strike
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(10);//strike
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(10);//strike
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(10);//strike
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(10);//strike
		//last round
		listOfTosses.add(9);
		listOfTosses.add(1);//spare
		listOfTosses.add(10);//strike
		
		return new Integer(200);
	}
	
	/**
	 * Generate toss data with no strikes and no spares
	 * @return
	 */
	public int generateTossWithNoBonus() {
		listOfTosses = new ArrayList<Integer>();
		int finalScore = 0;
		Random randInt = new Random();
		randInt.nextInt(10);//0 to 9: total pins = 10
		for(int i=0;i<20;i++) {
			if(i%2==0) {
				int tossPoints = randInt.nextInt(10);
				finalScore += tossPoints;
				listOfTosses.add(tossPoints);
			}else {
				int tossPoints = randInt.nextInt(10 - listOfTosses.get(i-1));
				finalScore += tossPoints;
				listOfTosses.add(tossPoints);
			}
		}
		return finalScore;
	}
}
